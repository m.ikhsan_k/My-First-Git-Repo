package tutorial.javari.service;

import java.util.List;

import tutorial.javari.animal.Animal;

public interface AnimalService {

    Animal findById(long id);

    //Animal findByName(String name);

    void saveAnimal(Animal animal);

    //void updateAnimal(Animal animal);

    void deleteAnimalById(long id);

    List<Animal> findAllAnimals();

    //void deleteAllAnimals();

    boolean isAnimalIdExist(Animal animal);

}
