package tutorial.javari.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;
import tutorial.javari.service.AnimalService;
import tutorial.javari.util.CustomErrorType;

@RestController
public class JavariController {

    public static final Logger logger = LoggerFactory.getLogger(JavariController.class);

    @Autowired
    AnimalService animalService;

    // -------------------Retrieve All Users---------------------------------------------

    @RequestMapping(value = "/javari/", method = RequestMethod.GET)
    public ResponseEntity<List<Animal>> listAllAnimals() {
        List<Animal> animals = animalService.findAllAnimals();
        if (animals == null) {
            String message = String.format("Currently, there are no animals.");
            logger.error(message);
            return new ResponseEntity(new CustomErrorType(message), HttpStatus.NOT_FOUND);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Animal>>(animals, HttpStatus.OK);
    }

    // -------------------Retrieve Single User------------------------------------------

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getAnimal(@PathVariable("id") int id) {
        logger.info("Fetching Animal with id {}", id);
        Animal animal = animalService.findById(id);
        if (animal == null) {
            String message = String.format("Animal with id " + id + " not found");
            logger.error(message);
            return new ResponseEntity(new CustomErrorType(message), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Animal>(animal, HttpStatus.OK);
    }

    // -------------------Create an Animal-------------------------------------------

    @RequestMapping(value = "/javari/", method = RequestMethod.POST)
    public ResponseEntity<?> createAnimal(@RequestBody Map<String, String> animalAttr,
                                          UriComponentsBuilder ucBuilder) {

        int id = Integer.parseInt(animalAttr.get("id"));
        String type = animalAttr.get("type");
        String name = animalAttr.get("name");
        Condition condition = Condition.parseCondition(animalAttr.get("condition"));
        double length = Double.parseDouble(animalAttr.get("length"));
        double weight = Double.parseDouble(animalAttr.get("weight"));
        Gender gender = Gender.parseGender(animalAttr.get("gender"));

        Animal animal = new Animal(id, type, name, gender, length, weight, condition);
        logger.info("Creating animal : {}", animal);

        if (animalService.isAnimalIdExist(animal)) {
            String message = String.format("Unable to create. An animal with id "
                    + animal.getId() + " already exists");
            logger.error(message);
            return new ResponseEntity(new CustomErrorType(message), HttpStatus.CONFLICT);
        }
        animalService.saveAnimal(animal);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/javari/{id}").buildAndExpand(animal.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    // ------------------- Delete a User-----------------------------------------

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAnimal(@PathVariable("id") int id) {
        logger.info("Fetching & Deleting User with id {}", id);

        Animal currentAnimal = animalService.findById(id);

        if (currentAnimal == null) {
            String message = String.format("Unable to delete. User with id " + id + " not found.");
            logger.error(message);
            return new ResponseEntity(new CustomErrorType(message), HttpStatus.NOT_FOUND);
        }
        animalService.deleteAnimalById(id);
        return new ResponseEntity<Animal>(HttpStatus.NO_CONTENT);
    }
}
