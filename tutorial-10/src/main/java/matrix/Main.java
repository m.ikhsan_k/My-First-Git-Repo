package matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    private static String genericMatrixPath = "plainTextDirectory/input/matrixProblem";
    private static String pathFileMatrix1 = genericMatrixPath + "A/matrixProblemSet1.txt";
    private static int numberOfLine1 = 50;

    private static String pathFileMatrix2 = genericMatrixPath + "A/matrixProblemSet2.txt";
    private static int numberOfLine2 = 50;

    private static String pathFileMatrixB1 = genericMatrixPath + "B/matrix10rows50columns.txt";
    private static int numberOfRow1 = 10;
    private static int numberOfCol1 = 50;

    private static String pathFileMatrixB2 = genericMatrixPath + "B/matrix50row10column.txt";
    private static int numberOfRow2 = 50;
    private static int numberOfCol2 = 10;

    private static double[][] multiplicationResult;

    public static void main(String[] args) throws
            IOException, InvalidMatrixSizeForMultiplicationException {

        //Convert into array
        double[][] firstMatrix = convertInputFileToMatrix(pathFileMatrix1, numberOfLine1);
        double[][] secondMatrix = convertInputFileToMatrix(pathFileMatrix2, numberOfLine2);

        ////////////////////////SQUARE MATRIX//////////////////////////
        long start = System.nanoTime();
        //Example usage of basic multiplication algorithm.
        multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(firstMatrix, secondMatrix);
        long end = System.nanoTime();
        long microseconds = (end - start) / 1000;
        System.out.println("Basic Matrix Multiplication Algorithm Complete in " + microseconds
                + " microseconds");

        //====================================
        start = System.nanoTime();
        //Example usage of strassen multiplication algorithm.
        double[][] strassenMultiplicationResult =
                MatrixOperation.strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Strassen Matrix Multiplication For Non Square Matrix "
                + "Complete in " + microseconds + " microseconds");

        System.out.println();
        /////////////////////NON SQUARE MATRIX///////////////////
        //Convert non square matrix into array
        double[][] firstNonSquareMatrix = convertInputFileToNonSquareMatrix(pathFileMatrixB1,
                numberOfRow1, numberOfCol1);
        double[][] secondNonSquareMatrix = convertInputFileToNonSquareMatrix(pathFileMatrixB2,
                numberOfRow2, numberOfCol2);

        start = System.nanoTime();
        //Example usage of basic multiplication algorithm.
        multiplicationResult =
                MatrixOperation.basicMultiplicationAlgorithm(secondNonSquareMatrix,
                        firstNonSquareMatrix);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Basic Non Square Matrix Multiplication Algorithm Complete in "
                + microseconds + " microseconds");

        //=====================================
        //start = System.nanoTime();
        ////Example usage of strassen multiplication algorithm.
        //strassenMultiplicationResult =
        //        MatrixOperation.strassenMatrixMultiForNonSquareMatrix(secondNonSquareMatrix,
        //                firstNonSquareMatrix);
        //end = System.nanoTime();
        //microseconds = (end - start) / 1000;
        //System.out.println("Strassen Matrix Multiplication For Non Square Matrix " +
        //        "Complete in " + microseconds + " microseconds");

    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @param numberOfLine the number of row (and possibly column) inside the square matrix.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static double[][] convertInputFileToMatrix(String pathFile, int numberOfLine)
            throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);
        double[][] matrix = new double[numberOfLine][numberOfLine];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfLine = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            matrix[indexOfLine] = sequenceIntoArray(currentLine);
            indexOfLine++;
        }
        return matrix;
    }

    /**
     * Converting a file input into an 2 dimensional array of double that represent a matrix.
     * @param pathFile is a path to file input.
     * @param numberOfRow the number of row inside the square matrix.
     * @param numberOfCol the number of column inside the square matrix.
     * @return 2 dimensional array of double representing matrix.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static double[][] convertInputFileToNonSquareMatrix(String pathFile, int numberOfRow,
                                                                int numberOfCol)
            throws IOException {
        File matrixFile = new File(pathFile);
        FileReader fileReader = new FileReader(matrixFile);
        double[][] matrix = new double[numberOfRow][numberOfCol];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfLine = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            matrix[indexOfLine] = sequenceIntoArray(currentLine);
            indexOfLine++;
        }
        return matrix;
    }

    /**
     * Converting a row of sequence of double into an array.
     * @param currentLine sequence of double from input representing a row from matrix.
     * @return array of double representing a row from matrix.
     */
    private static double[] sequenceIntoArray(String currentLine) {
        String[] arrInput = currentLine.split(" ");
        double[] arrInteger = new double[arrInput.length];
        for (int index = 0; index < arrInput.length; index++) {
            arrInteger[index] = Double.parseDouble(arrInput[index]);
        }
        return arrInteger;
    }
}
