package sorting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main2 {

    private static String pathFile = "plainTextDirectory/input/sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;

    public static void main(String[] args) throws IOException {

        int[] sequenceInput = convertInputFileToArray();

        /////////////SEARCHING BEFORE SORTING/////////////

        //Searching 1 Input Before Sorting
        long start = System.nanoTime();
        int searchingResultBeforeSort = Finder.slowSearch(sequenceInput, 40738);
        long end = System.nanoTime();
        long microseconds = (end - start) / 1000;
        System.out.println("Searching 1 Before Sorting Complete in " + microseconds
                + " microseconds");

        //Searching 2 Input Before Sorting
        start = System.nanoTime();
        int searchingResultBeforeSort2 = Finder.fasterSearch(sequenceInput, 40738);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Searching 2 Before Sorting Complete in " + microseconds
                + " microseconds");

        /////////////SORTING AND SEARCHING/////////////
        //////////FIRST METHOD//////////
        System.out.println();
        System.out.println("First Method");
        //Sorting Input 1
        start = System.nanoTime();
        int[] sortedInput = Sorter.slowSort(sequenceInput);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Sorting 1 Complete in " + microseconds + " microseconds");

        //Searching 1 Input After Sorting
        start = System.nanoTime();
        int searchingResultAfterSort = Finder.slowSearch(sequenceInput, 40738);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Searching 1 After Sorting Complete in " + microseconds
                + " microseconds");

        sequenceInput = convertInputFileToArray();

        //////////SECOND METHOD//////////
        System.out.println();
        System.out.println("Second Method");
        //Sorting Input 2
        start = System.nanoTime();
        int[] sortedInput2 = Sorter.quickSort(sequenceInput, 0,
                sequenceInput.length - 1);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Sorting 2 Complete in " + microseconds
                + " microseconds");

        //Searching 2 Input After Sorting
        start = System.nanoTime();
        int searchingResultAfterSort2 = Finder.fasterSearch(sequenceInput,
                40738);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Searching 2 After Sorting Complete in " + microseconds
                + " microseconds");

        //Searching 2 Input After Sorting
        start = System.nanoTime();
        int searchingResultAfterSort2v1 = Finder.binarySearch(sequenceInput,
                40738);
        end = System.nanoTime();
        microseconds = (end - start) / 1000;
        System.out.println("Searching 2 After Sorting With Binary Search Complete in "
                + microseconds + " microseconds");

    }

    /**
     * Converting a file input into an array of integer.
     * @return an array of integer that represent an integer sequence.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static int[] convertInputFileToArray() throws IOException {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        int[] sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        return sequenceInput;
    }
}
