package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class HerveCheese implements Cheese {

    public String toString() {
        return "Herve Cheese";
    }
}