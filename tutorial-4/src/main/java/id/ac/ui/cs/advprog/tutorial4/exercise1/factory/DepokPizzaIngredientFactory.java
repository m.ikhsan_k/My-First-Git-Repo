package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.HerveCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.RazorClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MediumCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.GravySauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kale;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kuka;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Orache;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new MediumCrustDough();
    }

    public Sauce createSauce() {
        return new GravySauce();
    }

    public Cheese createCheese() {
        return new HerveCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Kale(), new Kuka(), new Orache(), new Spinach()};
        return veggies;
    }

    public Clams createClam() {
        return new RazorClams();
    }
}
