package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        this.name = name;
        if (salary < 70000) {
            throw new IllegalArgumentException("Security Expert salary must be greater than 70000");
        } else {
            this.salary = salary;
        }
        role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
