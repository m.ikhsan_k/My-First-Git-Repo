package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary < 90000) {
            throw new IllegalArgumentException("Ui Ux Designer salary must be greater than 90000");
        } else {
            this.salary = salary;
        }
        role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
