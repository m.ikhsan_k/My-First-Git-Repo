import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getName() {
        Customer customer = new Customer("Alice");

        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        Customer customer = new Customer("Alice");
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void htmlStatementWithMultipleMovies() {
        // TODO Implement me!
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.NEW_RELEASE);
        Rental rent = new Rental(movie, 2);
        
        Customer customer = new Customer("Alice");
        customer.addRental(rent);

        movie = new Movie("Test", Movie.CHILDREN);
        rent = new Rental(movie, 4);

        customer.addRental(rent);

        movie = new Movie("Test2", Movie.REGULAR);
        rent = new Rental(movie, 1);

        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("<tr>");
        
        assertEquals(4, lines.length);
        assertTrue(result.contains("<p>Amount owed is <em>11"));
        assertTrue(result.contains("<em>4</em> frequent renter points</p>"));
    }
}