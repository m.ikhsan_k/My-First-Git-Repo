import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.Test;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getTitle() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void equalsItselfs() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertTrue(movie.equals(movie));
    }

    @Test
    public void equalsNull() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);

        assertFalse(movie.equals(null));
    }

    @Test
    public void equalsAnotherMovie() {
        Movie movie = new Movie("Who Killed Captain Alex?1", Movie.REGULAR);
        Movie movie2 = new Movie("Who Killed Captain Alex?2", Movie.CHILDREN);

        assertFalse(movie.equals(movie2));

        movie2 = new Movie("Who Killed Captain Alex?1", Movie.REGULAR);

        assertTrue(movie.equals(movie2));
    }

    @Test
    public void testHash() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        assertEquals(Objects.hash(movie.getTitle(), movie.getPriceCode()), movie.hashCode());
    }
}