import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        String result = "Rental Record for " + getName() + "\n";

        for (Rental each : rentals) {
            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" + each.getAmount() + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + getTotalAmount() + "\n";
        result += "You earned " + getTotalFrequentRenterPoints() + " frequent renter points";

        return result;
    }
    
    public String htmlStatement() {
        String result = "<h1>Rental Record for <em>" + getName() + "</em></h1>";
        result += "<table><tbody>";
        for (Rental each : rentals) {
            // Show figures for this rental
            result += "<tr><td>" + each.getMovie().getTitle() + "</td><td>" + each.getAmount() 
                + "</td></tr>";
        }
        result += "</tbody></table>";
        result += "<p>Amount owed is <em>" + getTotalAmount() + "</em></p>";
        result += "<p>You earned <em>" + getTotalFrequentRenterPoints() 
            + "</em> frequent renter points</p>";

        return result;
    }
    
    private double getTotalAmount() {
        double totalAmount = 0;
        for (Rental each : rentals) {
            totalAmount += each.getAmount();
        }
        return totalAmount;
    }
    
    private int getTotalFrequentRenterPoints() {
        int frequentRenterPoints = 0;
        for (Rental each : rentals) {
            frequentRenterPoints += each.getFrequentRenterPoints();
        }
        return frequentRenterPoints;
    }
}