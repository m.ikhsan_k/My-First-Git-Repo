package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior{
    // TODO Complete me!
	@Override
    public void fly() {
        System.out.println("Yes! My wings are perfect, I can fly!");
    }

}
